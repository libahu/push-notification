# API Webhook Documentation

## Domain
> Domain:  https://beta-faucet-api.dev.chainverse.xyz

## API 


### Api Webhook push notification
> Description: This is an api for ghost server that can call the CV noti server. Thereby, CV Noti Server pushes notifications to the client via firebase

- Version: `{1.0}`
- Endpoint: `/v1/screehvain/webhook-notification`
- Authentication: none
- Method: `POST`
- Body Parameters: 

| Name                       | Require? |                Type |                                      Description |
| -------------------------- | :------: | ------------------: | -----------------------------------------------: |
| `norifications`            |   [x]    | `Array Json object` |         List noti token and message want to push |
| `norifications.message`    |   [x]    |            `String` |                   Message want to push to client |
| `norifications.noti_token` |   [x]    |            `String` | The notification token is provided by the client |

- Response: `JSON Object`

| Name      |   Type   |      Description |
| --------- | :------: | ---------------: |
| `message` | `String` | Message response |

- Status Codes

| Status  |                                                                                  Description |
| ------- | -------------------------------------------------------------------------------------------: |
| **200** |                                                                   The request was successful |
| **500** | The server encountered an unexpected condition that prevented it from fulfilling the request |


## Example

> Example call api. Import wallet to ghostr message app then call api: 

- app  ghostr version:

| Name      |       Type        |
| --------- | :---------------: |
| `android` | `Version V1.0.15` |
| `ios`     | `Version V1.0.17` |

- private key: `8e3d88002dae5fd6076e8e6844af5200845d57e2e0c4c94eda0165d4ffa5fd02`

- curl: 

```
curl  -X POST \
  'https://beta-faucet-api.dev.chainverse.xyz/v1/screehavin/webhook-notification' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "notifications": [
    {
      "message": "CtsBeyJkZXZpY2VfbnVtYmVyIjoiMkNFMTg5RTktNzdEMy00MzJBLTkzMUEtNTQxOEZCQkIwQTQ2IiwibWVzc2FnZV9zZW5kZXIiOiIiLCJyZWNpcGllbnQiOiIweDBGREEwREFlNTVhMTVBYTM4N0MzZjg5NWZCZjkyOTgyOTZmRUZhODkiLCJzZW5kZXIiOiIweDA2MGU0NkY2NThBMzc1NjRCNTE5QWI1MUVhYmEwMDBhMDdiODExQkIiLCJ0aW1lc3RhbXAiOjE2OTkyNTM4NDIsInR5cGUiOjB9EgRPa2llGoIBMzliZWU2YmM1MDkyZTcyMWQyODI3NmQyOTQyNmM0YzdhMDA5OGE1NDdjNTQ5ODQ0ZTdhZDA5NDE5OTExOTAyZDQ5ZWJlMzgzMzYzMzA4ZWYwZGNmZjZjYWU3MzVjMmI2OTYyNTEyNzgwNDgzZmY3YTY1NzIxMzYwOWI2NTg1MGIxYw",
      "noti_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiN2NlRTIwNGRENjUzNUI3ZkM2MGNBNzNEMUFmNDVCZDY1N0I3NTA1IiwicGVybWlzc2lvbiI6IlBVU0hfTk9USUZJQ0FUSU9OIiwiYXBwIjoiZ2hvc3RfbWVzc2FnZSIsInNlcnZlcl91cmkiOiJnb29nbGUuY29tIiwiaWF0IjoxNjk5OTQ2NjQ4LCJleHAiOjE3MDA1NTE0NDh9.f9ek1FRd4XFZzHJ8ZsSOoBYjNuqzp6t1Os9Ho9elqUI"
    },
    {
      "message": "CtsBeyJkZXZpY2VfbnVtYmVyIjoiMkNFMTg5RTktNzdEMy00MzJBLTkzMUEtNTQxOEZCQkIwQTQ2IiwibWVzc2FnZV9zZW5kZXIiOiIiLCJyZWNpcGllbnQiOiIweDBGREEwREFlNTVhMTVBYTM4N0MzZjg5NWZCZjkyOTgyOTZmRUZhODkiLCJzZW5kZXIiOiIweDA2MGU0NkY2NThBMzc1NjRCNTE5QWI1MUVhYmEwMDBhMDdiODExQkIiLCJ0aW1lc3RhbXAiOjE2OTkyNTM4NDIsInR5cGUiOjB9EgRPa2llGoIBMzliZWU2YmM1MDkyZTcyMWQyODI3NmQyOTQyNmM0YzdhMDA5OGE1NDdjNTQ5ODQ0ZTdhZDA5NDE5OTExOTAyZDQ5ZWJlMzgzMzYzMzA4ZWYwZGNmZjZjYWU3MzVjMmI2OTYyNTEyNzgwNDgzZmY3YTY1NzIxMzYwOWI2NTg1MGIxYw",
      "noti_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiN2NlRTIwNGRENjUzNUI3ZkM2MGNBNzNEMUFmNDVCZDY1N0I3NTA1IiwicGVybWlzc2lvbiI6IlBVU0hfTk9USUZJQ0FUSU9OIiwiYXBwIjoiZ2hvc3RfbWVzc2FnZSIsInNlcnZlcl91cmkiOiJnb29nbGUuY29tIiwiaWF0IjoxNjk5OTQ2NjQ4LCJleHAiOjE3MDA1NTE0NDh9.f9ek1FRd4XFZzHJ8ZsSOoBYjNuqzp6t1Os9Ho9elqUI"
    }
  ]
}'
```